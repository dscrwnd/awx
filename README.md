# AWX DOCKER-COMPOSE 

Run awx in docker-compose without having to do those super weird things from the official repo:

https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md

## PREREQUISITES:

```
docker
docker-compose
```

just clone this repo and execute:

```bash
cd docker-compose
docker-compose up -d 
# WAIT A LITTLE SO EVERYTHING RUNS
docker exec tools_awx_1 make clean-ui ui-devel # builds the UI, in the future will use the new UI.
docker exec -ti tools_awx_1 awx-manage createsuperuser # ( this will ask some stuff to create the admin user of awx ).
```

after this, you can access awx in : https://YOUR-HOST-IP:8043/#/home

```
images for AWX_TOOLS_1 from here:

https://github.com/ansible/awx/pkgs/container/awx_devel

```